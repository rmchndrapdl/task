<?php

Route::get('/', function () { return redirect('login'); });

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('tasks', 'TaskController@index');
    Route::post('tasks/{id}/update', 'TaskController@update')->name('task.update');    
    Route::post('tasks', 'TaskController@store')->name('task.store');    
});