<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class UserAssignTaskType extends Model
{
    protected $table = 'user_assign_task_type';
    
    protected $fillable = ['user_id', 'task_type_id'];


    public function taskType()
    {
    	return $this->belongsTo('App\Models\Tasks\TaskType');
    }  

}
