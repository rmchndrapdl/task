<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    
    protected $fillable = ['task_type_id', 'title','iscompleted','completed_by', 'created_at', 'updated_at'];

    public function taskType()
    {
    	return $this->belongsTo('App\Models\Tasks\TaskType');
    }
}
