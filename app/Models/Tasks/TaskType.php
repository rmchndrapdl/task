<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
    protected $table = 'tasks_type';
    
    protected $fillable = ['title', 'created_at', 'updated_at'];
}
