<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tasks\Task;
use App\Models\Tasks\UserAssignTaskType;
use App\Models\Tasks\TaskType;
use DB;

class TaskController extends Controller
{
    public function index()
    {
        $user_id = \Auth::user()->id;        
        $user_assign_task_type = UserAssignTaskType::select('task_type_id')->groupBy('task_type_id')->where('user_id','=', $user_id)->orderBy('created_at', 'desc')->get();
        $tasks = Task::whereIn('task_type_id', $user_assign_task_type)->where('iscompleted',0)->orderBy('created_at', 'desc')->get();
        $tasks_picked = Task::where('iscompleted',1)->where('completed_by',$user_id)->orderBy('created_at', 'desc')->get();
        $task_type = TaskType::all();
        return view('tasks.index', compact('tasks', 'user_assign_task_type','tasks_picked','task_type'));
    }

    public function store(Request $request)
    {
        $task = $request->all();
        Task::create($task);
        $request->session()->flash('message', 'Task Added successfully !');
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->fill($request->all())->save();
        $request->session()->flash('message', 'Task Picked successfully !');
        return redirect()->back();
    }
}
