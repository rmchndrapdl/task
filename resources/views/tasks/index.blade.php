@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @if(Auth::user()->id !== 4)
            <div class="panel panel-default">
                <div class="panel-heading">Tasks Dashboard</div>
                <div class="panel-body">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                @endif
                    @if(isset($user_assign_task_type))
                  <h3><strong>You have following task type</strong></h3>
                        @foreach($user_assign_task_type as $task)
                            <li>{{ $task->taskType->title }}</li>
                        @endforeach
                    @else
                        There is no task.
                    @endif
                <hr>
                <h3><strong>Doing / Picked Task</strong></h3>                  
                    @if(isset($tasks_picked))
                        @foreach($tasks_picked as $task)
                            <li>{{ $task->title }}</li>
                        @endforeach
                    @else
                        There is no task.
                    @endif
                  <hr>
                    @if(isset($tasks))
                  <h3><strong>All Tasks</strong></h3>
                        @foreach($tasks as $task)
                            <li>{{ $task->title }} 
                                <form method="post" action="{{ route('task.update',['id' => $task->id]) }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                                    <input type="hidden" name="iscompleted" value=1 >
                                    <input type="hidden" name="completed_by" value="{{ Auth::user()->id }}" >
                                    <input class="btn btn-primary" type="submit" value="Pick / Do Task">                                                              
                                </form> 
                            </li>
                            <br>
                        @endforeach
                    @else
                        There is no task.
                    @endif
                </div>
            </div>
        @else

        @if(Session::has('message'))
            <div class="alert alert-success">
                {{ Session::get('message') }}
            </div>
        @endif
        <form action="{{ route('task.store') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" >

            <div class="col-ls-10">
                <select class="form-control m-b" name="task_type_id">
                    @foreach($task_type as $data)
                        <option value="{{ $data->id }}"> {{ $data->title }} </option>
                    @endforeach
                </select>
            </div>
            <br>
            <div>
                <input class="form-control" type="text" name="title" placeholder="Enter Task Title">
            </div>
            <br>
            <button class="btn btn-success" type="submit">                
                Save
            </button>
        </form>
        @endif
        </div>
    </div>
</div>
@endsection
