<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    public function up()
    {
      Schema::create('tasks_type', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->timestamps();
      });

      Schema::create('tasks', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('task_type_id')->unsigned();
          $table->integer('completed_by')->unsigned();
          $table->string('title');
          $table->tinyInteger('iscompleted')->default(0);	          
          $table->timestamps();

          $table->index('task_type_id');
      });
    }

    public function down()
    {
        Schema::drop('tasks_type');
        Schema::drop('tasks');
    }
}
