<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignUserTaskTable extends Migration
{
    public function up()
    {
      Schema::create('user_assign_task_type', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->unsigned();
          $table->integer('task_type_id')->unsigned();
          $table->timestamps();

          $table->engine = 'InnoDB';
      });
    }

    public function down()
    {
      Schema::drop('user_assign_task_type');
    }
}
