<?php

use Illuminate\Database\Seeder;

class TasksTableSeed extends Seeder
{
    public function run()
    {
      DB::table('tasks_type')->insert([
          'title' => 'Task Type 1',
      ]);
      
      DB::table('tasks_type')->insert([
          'title' => 'Task Type 2',
      ]);

      DB::table('tasks')->insert([
          'task_type_id' => 1,
          'title' => 'Task Title 1',
      ]);

      DB::table('tasks')->insert([
          'task_type_id' => 1,
          'title' => 'Task Title 2',
      ]);

      DB::table('tasks')->insert([
          'task_type_id' => 2,
          'title' => 'Task Title 3',
      ]);
    }
}
