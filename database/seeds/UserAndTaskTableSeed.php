<?php

use Illuminate\Database\Seeder;

class UserAndTaskTableSeed extends Seeder
{
    public function run()
    {
      DB::table('user_assign_task_type')->insert([
          'user_id' => 1,
          'task_type_id' => 1,
      ]);
      DB::table('user_assign_task_type')->insert([
          'user_id' => 1,
          'task_type_id' => 2,
      ]);
      DB::table('user_assign_task_type')->insert([
          'user_id' => 2,
          'task_type_id' => 1,
      ]);
      DB::table('user_assign_task_type')->insert([
          'user_id' => 3,
          'task_type_id' => 2,
      ]);
    }
}
