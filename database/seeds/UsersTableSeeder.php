<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User 1',
            'email' => 'user@one.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'User 2',
            'email' => 'user@two.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'User 3',
            'email' => 'user@three.com',
            'password' => bcrypt('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Task Creator',
            'email' => 'user@tc.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
